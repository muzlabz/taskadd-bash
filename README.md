# Taskadd Bash

Check list for terminal. [.bashrc]

<><><><><><><><><><><><><><><><><><><><><><><><><><><><>

 put code bashtask in your .bashrc or .bash_aliases etc.

<><><><><><><><><><><><><><><><><><><><><><><><><><><><>

##### how to

- `taskadd` taskadd "Go grocery shopping"
- `taskin`  Insert a task between items  : $ taskin 3 'hello world'
- `taskrm`  taskrm 2 --> Removes second item in list
- `taskcl`  Delete and create a new taskfile

